package com.example.simplegameengine;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class gameActivity extends AppCompatActivity {

    GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        gameView = new GameView (this);
        setContentView(gameView);
    }

    class GameView extends SurfaceView implements Runnable {

        Thread gameThread = null;

        SurfaceHolder ourHolder;

        volatile boolean playing;

        Canvas canvas;
        Paint paint;

        long fps;

        private long timeThisFrame;

        Bitmap bitmapBob;

        boolean isMoving = false;

        boolean maxFrame = false;

        float walkSpeedPerSecond = 150;

        float bobXpos = 10;

        public GameView(Context context) {
            super(context);

            ourHolder = getHolder();
            paint = new Paint();

            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob);
            playing = true;

        }

        @Override
        public void run() {
            while (playing){
                long startFrameTime = System.currentTimeMillis();

                update();

                draw();

                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame > 0){
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        public void update() {
            if (isMoving){
                if (bobXpos <= 0) {
                    maxFrame = false;
                }
                else if (bobXpos >= 1080 - bitmapBob.getWidth()) {
                    maxFrame = true;
                }

                if (maxFrame) {
                    bobXpos = bobXpos - (walkSpeedPerSecond / fps);
                }
                else {
                    bobXpos = bobXpos + (walkSpeedPerSecond / fps);
                }
            }
        }

        public void draw() {
            if (ourHolder.getSurface().isValid()) {
                canvas = ourHolder.lockCanvas();
                canvas.drawColor(Color.RED);
                paint.setColor(Color.WHITE);
                paint.setTextSize(45);
                canvas.drawText("FPS : " + fps, 20 , 40, paint);
                canvas.drawBitmap(bitmapBob, bobXpos, 200, paint);

                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public void pause() {
            playing = false;
            try {
                gameThread.join();
            }catch (InterruptedException e) {
                Log.e("Error : ", "Joining Thread");
            }
        }

        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    isMoving = true;
                    break;

                case MotionEvent.ACTION_UP:
                    isMoving = false;
                    break;
            }
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        gameView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }
}
